#include <stdio.h>
#include <time.h>
#include <windows.h>
#include <conio.h> //getch()

const int weight=8;//change size of border
const char CPPKEYUP = 72;
const char CPPKEYLEFT = 75;
const char CPPKEYRIGHT = 77;
const char CPPKEYDOWN = 80;
const char ENTER = 13;

int board [weight][weight];//border arr
bool possibleMovies[weight][weight];//possible move arr
char player1='#';//charector for player
char player2='o';
char validmove='*';
int colorplayer1=1;//color code player
int colorplayer2=2;
int colorborder=3;
int validmovecoler=4;
char column='-';//border charector
char row='|';
char name1[100];//name player charector
char name2[100];

//move in console
void gotoxy(int x, int y){
 HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
 COORD cursorCoord;
 cursorCoord.X=x;
 cursorCoord.Y=y;
 SetConsoleCursorPosition(consoleHandle, cursorCoord);
}
//clear screen of console
void clrscr(){
 system("cls");
}
//change color of text in console
void setTextColor(int textColor, int backColor=0){
 HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
 int colorAttribute = backColor << 4 | textColor;
 SetConsoleTextAttribute(consoleHandle, colorAttribute);
}
//sleep for time!
void sleep(unsigned int mseconds)
{
 clock_t goal = mseconds + clock();
 while (goal > clock());
}
//reverse coins when on coins add in border
void ReversePoints(int player,int i,int j){
        int mi , mj;
        int oplayer = ((player == 1) ? 2 : 1);

        //move up
        bool swup=false;
        mi = i - 1;
        mj = j;
        while(mi>0 && board[mi][mj] == oplayer){
            mi--;
        }
        if(mi>=0 && board[mi][mj] == player){
            swup=true;
        }
        mi = i - 1;
        mj = j;
        while(mi>0 && board[mi][mj] == oplayer && swup ){
            board[mi][mj]=player;
            mi--;
        }


        //move down
        bool swdown=false;
        mi = i + 1;
        mj = j;
        while(mi<weight && board[mi][mj] == oplayer){
            mi++;
        }
        if(mi<=weight && board[mi][mj] == player){
            swdown=true;
        }
        mi = i + 1;
        mj = j;
        while(mi<weight && board[mi][mj] == oplayer && swdown){
            board[mi][mj]=player;
            mi++;
        }

        //move left
        bool swleft=false;
        mi = i;
        mj = j - 1;
        while(mj>0 && board[mi][mj] == oplayer){
            mj--;
        }
        if(mj>=0 && board[mi][mj] == player){
            swleft=true;
        }
        mi = i;
        mj = j - 1;
        while(mj>0 && board[mi][mj] == oplayer && swleft){
            board[mi][mj]=player;
            mj--;
        }
        //move right
        bool swright=false;
        mi = i;
        mj = j + 1;
        while(mj<weight && board[mi][mj] == oplayer){
            mj++;
        }
        if(mj<=weight && board[mi][mj] == player){
            swright=true;
        }
        mi = i;
        mj = j + 1;
        while(mj<weight && board[mi][mj] == oplayer && swright){
            board[mi][mj]=player;
            mj++;
        }

        //move up left
        bool swupleft=false;
        mi = i - 1;
        mj = j - 1;
        while(mi>0 && mj>0 && board[mi][mj] == oplayer){
            mi--;
            mj--;
        }
        if(mi>=0 && mj>=0 && board[mi][mj] == player){
            swupleft=true;
        }
        mi = i - 1;
        mj = j - 1;
        while(mi>0 && mj>0 && board[mi][mj] == oplayer && swupleft){
            board[mi][mj]=player;
            mi--;
            mj--;
        }

        //move up right
       bool swupright=false;
        mi = i - 1;
        mj = j + 1;
        while(mi>0 && mj<weight && board[mi][mj] == oplayer){
            mi--;
            mj++;
        }
        if(mi>=0 && mj<=weight && board[mi][mj] == player){
            swupright=true;
        }
        mi = i - 1;
        mj = j + 1;
        while(mi>0 && mj<weight && board[mi][mj] == oplayer && swupright){
            board[mi][mj]=player;
            mi--;
            mj++;
        }

        //move down left
        bool swdownleft=false;
        mi = i + 1;
        mj = j - 1;
        while(mi<weight && mj>0 && board[mi][mj] == oplayer){
            mi++;
            mj--;
        }
        if(mi<=weight && mj>=0 && board[mi][mj] == player){
            swdownleft=true;
        }
        mi = i + 1;
        mj = j - 1;
        while(mi<weight && mj>0 && board[mi][mj] == oplayer && swdownleft){
            board[mi][mj]=player;
            mi++;
            mj--;
        }

        //move down right
        bool swdownright=false;
        mi = i + 1;
        mj = j + 1;
        while(mi<weight && mj<weight && board[mi][mj] == oplayer){
            mi++;
            mj++;
        }
        if(mi<=weight && mj<=weight && board[mi][mj] == player){
            swdownright=true;
        }
        mi = i + 1;
        mj = j + 1;
        while(mi<weight && mj<weight && board[mi][mj] == oplayer && swdownright){
            board[mi][mj]=player;
            mi++;
            mj++;
        }

}
// 
bool canPlay(int player,int i,int j){
        if(board[i][j] != 0) return false;

        int mi , mj , c;
        int oplayer = ((player == 1) ? 2 : 1);

        //move up
        mi = i - 1;
        mj = j;
        c = 0;
        while(mi>0 && board[mi][mj] == oplayer){
            mi--;
            c++;
        }
        if(mi>=0 && board[mi][mj] == player && c>0) return true;


        //move down
        mi = i + 1;
        mj = j;
        c = 0;
        while(mi<weight && board[mi][mj] == oplayer){
            mi++;
            c++;
        }
        if(mi<=weight && board[mi][mj] == player && c>0) return true;

        //move left
        mi = i;
        mj = j - 1;
        c = 0;
        while(mj>0 && board[mi][mj] == oplayer){
            mj--;
            c++;
        }
        if(mj>=0 && board[mi][mj] == player && c>0) return true;

        //move right
        mi = i;
        mj = j + 1;
        c = 0;
        while(mj<weight && board[mi][mj] == oplayer){
            mj++;
            c++;
        }
        if(mj<=weight && board[mi][mj] == player && c>0) return true;

        //move up left
        mi = i - 1;
        mj = j - 1;
        c = 0;
        while(mi>0 && mj>0 && board[mi][mj] == oplayer){
            mi--;
            mj--;
            c++;
        }
        if(mi>=0 && mj>=0 && board[mi][mj] == player && c>0) return true;

        //move up right
        mi = i - 1;
        mj = j + 1;
        c = 0;
        while(mi>0 && mj<weight && board[mi][mj] == oplayer){
            mi--;
            mj++;
            c++;
        }
        if(mi>=0 && mj<=weight && board[mi][mj] == player && c>0) return true;

        //move down left
        mi = i + 1;
        mj = j - 1;
        c = 0;
        while(mi<weight && mj>0 && board[mi][mj] == oplayer){
            mi++;
            mj--;
            c++;
        }
        if(mi<=weight && mj>=0 && board[mi][mj] == player && c>0) return true;

        //move down right
        mi = i + 1;
        mj = j + 1;
        c = 0;
        while(mi<weight && mj<weight && board[mi][mj] == oplayer){
            mi++;
            mj++;
            c++;
        }
        if(mi<=weight && mj<=weight && board[mi][mj] == player && c>0) return true;

        //can't put coins here! 
        return false;
}
//find all possible moves for one player
void setAllPossibleMoves(int player){
    //clean arr
    for (int i = 0; i < weight; i++) {
            for (int j = 0; j < weight; j++) {
                    possibleMovies[i][j]=false;
            }
        }
        //find all posible place for move
        for (int i = 0; i < weight; i++) {
            for (int j = 0; j < weight; j++) {
                if(canPlay(player,i,j)){
                    possibleMovies[i][j]=true;
                }
            }
        }
}
//ready game for start!
void getStartBoard(){
        for (int i = 0; i < weight; i++) {
            for (int j = 0; j < weight; j++) {
                board[i][j] = 0;
            }
        }
        board[weight/2-1][weight/2-1] = 2;
        board[weight/2-1][weight/2] = 1;
        board[weight/2][weight/2-1] = 1;
        board[weight/2][weight/2] = 2;
}
//count of coins player!
int getPlayerStoneCount(int player){
        int score = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if(board[i][j] == player) score++;
            }
        }
        return score;
}
//player has any move or not?
bool hasAnyMoves(int player){
    setAllPossibleMoves(player);
    //move for all border
    for (int i = 0; i < weight; i++) {
        for (int j = 0; j < weight; j++) {
                if(possibleMovies[i][j])
                    return true;
        }
    }
    return false;
}
//if any player can't move game end!
bool isGameFinished(){
       return !(hasAnyMoves(1) || hasAnyMoves(2));
}
//find state of game! contine or not!
int getWinner(){
        if(!isGameFinished())
            //game not finished
            return -1;
        else{
            //count stones
            int p1s = getPlayerStoneCount(1);
            int p2s = getPlayerStoneCount(2);

            if(p1s == p2s){
                //tie
                return 0;
            }else if(p1s > p2s){
                //p1 wins
                return 1;
            }else{
                //p2 wins
                return 2;
            }
        }
    }
//sum all coins    
int getTotalStoneCount(){
        int c = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if(board[i][j] != 0) c++;
            }
        }
        return c;
}
//change turn of game!
void changeTurn(int &turn){
    if(turn==1){
        turn=2;
    }else {//turn==2
        turn=1;
    }
}
//print border of game ! and details of game
void printBorder(int turn){
	clrscr();
    for (int i=0;i<=3*weight;i++) {//move on ->
        for (int j=0;j<=2*weight;j++) {//move on |
            if(i%3==0){
                setTextColor(colorborder);
               printf("%c",column);
            }else if(i%3==1){
                if(j%2==0){
                 setTextColor(colorborder);
                 printf("%c",row);
                }else {//chap bord
                    if(possibleMovies[i/3][j/2]){
                        setTextColor(validmovecoler);
                        printf("%c",validmove);
                    }else {
                        if(board[i/3][j/2]==1){
                            setTextColor(colorplayer1);
                            printf("%c",player1);
                        }else if (board[i/3][j/2]==2) {
                            setTextColor(colorplayer2);
                            printf("%c",player2);
                        }else {
                           printf(" ");
                        }
                    }
                }
            }else {
                setTextColor(colorborder);
                if(j%2==0){
                 printf("%c",row);
                }else {
                  printf(" ");
                }
            }
        }
        printf("\n");
    }
    //print bord details:
    setTextColor(colorborder);
    int p1s = getPlayerStoneCount(1);
    int p2s = getPlayerStoneCount(2); 
    gotoxy(2*weight+10,weight-1);
    if(turn==1){
    	printf("turn : %s",name1);
	}else{
	    printf("turn : %s",name2);
	}
	 gotoxy(2*weight+10,weight);
	 printf("score of %s is: %d and her/his charactor is %c",name1,p1s,player1);
     gotoxy(2*weight+10,weight+1);
	 printf("score of %s is: %d and her/his charactor is %c",name2,p2s,player2);
}
//check the move is valid of not!
bool validMover(int x,int y){
    return possibleMovies[x][y];
}
//main code for player and player game!
int playgame()
{
	clrscr();
    getStartBoard();
    printf("enter player1 name:\n");
    scanf("%s",&name1);//gets() if need avoid space!
    printf("enter player2 name:\n");
    scanf("%s",&name2);
    char Arrow = 0;//input keybord1
    int turn=1;//show turn player 1 show that player 1 has to play
    int x=0,y=0;//postions of (x,y)
    while (getWinner()==-1) {
        setAllPossibleMoves(turn);//find all player possible move
        printBorder(turn);//print border!
        do{
            if(!hasAnyMoves(turn))//if player has't any move change turn!
              break;
            Arrow=0;
            while(Arrow != ENTER) {
            gotoxy(y*2+1,x*3+1);
            Arrow = getch();
            switch(Arrow) {
            case CPPKEYUP:
            if (x>0)
            {
                x--;
            }
            break; 
            case CPPKEYDOWN:
            if (x<weight-1)
            {
                x++;
            }
            break;
            case CPPKEYLEFT:
            if (y>0)
            {
                y--;
            }
            break;
            case CPPKEYRIGHT:
            if (y<weight-1)
            {
                y++;
            }
            break;
            }
        }
        }while(!validMover(x,y));//continue until the player select valid positon!
        if(validMover(x,y)){
        	board[x][y]=turn;
		}
		ReversePoints(turn,x,y);//revrse all coins!
        changeTurn(turn);
    }
    //clean screan
    printBorder(turn);
    gotoxy(0,3*weight+1);
    int winer=getWinner();
    if(winer==0){
    	printf("tie!!");
	}else if(winer==1){
		printf("%s win! wow",name1);
	}else{//winer==2
		printf("%s win! wow",name2);
	}
    sleep(5000);
}
//move in console and select item!
int SelectedByKey(int max){//max =lines of code!
	 char Arrow = 0;
	 int x=0;
	 while(Arrow != ENTER) {
            gotoxy(0,x);
            Arrow = getch();
            switch(Arrow) {
            case CPPKEYUP:
            if (x>0)
            {
                x--;
            }
            break; 
            case CPPKEYDOWN:
            if (x<max-1)
            {
                x++;
            }
            break;
        }
    }
    return x;
}
//print color for player choose!
int GenerateColor(){
	clrscr();
	for(int i=0;i<16;i++){
	setTextColor(i);
	printf("%d)sample \n",i+1);
	}
	return SelectedByKey(16);
}
//print charector for player select from them!
char GenerateCharector(){
	clrscr();
	setTextColor(colorborder);
	char list[]={'*','8','&','%','#','$','@','!','p','m'};
	int sizelist=10;//size list !!
	for(int i=0;i<sizelist;i++){
	printf("%d) %c \n",i+1,list[i]);
	}
	return list[SelectedByKey(sizelist)];
}
//settin menu!
void settings(){
	while(true){
	int pos;
	setTextColor(colorborder);
	clrscr();
	printf("1)setcolorplayer1 \n2)setcolorplayer2 \n3)setcolorBorderandsetting \n4)setvalidmovecolor \n5)setcharectorplayer1 \n6)setcharectorplayer2 \n7)validmovecharector \n8)back");
	pos=SelectedByKey(8);
	if(pos==0){
	colorplayer1=GenerateColor();
	}else if(pos==1){
		colorplayer2=GenerateColor();
	}else if(pos==2){
		 colorborder=GenerateColor();
	}else if(pos==3){
		validmovecoler=GenerateColor();
	}else if(pos==4){
		player1=GenerateCharector();
	}else if(pos==5){
		player2=GenerateCharector();
	}else if(pos==6){
		validmove=GenerateCharector();
	}else{//pos==7
		break;
	}
	}
}
//menu game!
void menu(){
	while(true){
	int pos;
	setTextColor(colorborder);
	clrscr();
	printf("1)playgame \n2)menu \n3)about \n4)exit \n");
	pos=SelectedByKey(4);
	if(pos==0){
	playgame();
	}else if(pos==1){
		settings();
	}else if(pos==2) {//pos==2
		clrscr();
		printf("writer : Mahdi Akbari Zarkesh");
		sleep(1000);
	}else{
		clrscr();
		printf("bye bye!!");
		sleep(1000);
		break;
	}
	}
}
//code start form here!
int main(){
	menu();
	return 0;
}
//good luck :)